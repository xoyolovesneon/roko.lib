﻿using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace ROKO.Lib.BLL.Tests
{
    [TestFixture]
    public class ValidatorTest
    {
        private BookValidator bookValidator;
        private NewspaperValidator newspaperValidator;

        [SetUp]
        public void Startup()
        {
            var fakeLoggerBookValidator = new Mock<ILogger<BookValidator>>();
            var fakeLoggerNewspaperValidator = new Mock<ILogger<NewspaperValidator>>();

            bookValidator = new BookValidator(fakeLoggerBookValidator.Object);
            newspaperValidator = new NewspaperValidator(fakeLoggerNewspaperValidator.Object);
        }

        [Test]
        public void ValidatorTest_IsFirstLatinUpper_True()
        {
            string test = "First symbol is Upper";

            Assert.IsTrue(bookValidator.IsFirstUpper(test));
        }

        [Test]
        public void ValidatorTest_IsFirstLatinUpper_False()
        {
            string test = "first symbol is Upper";

            Assert.IsFalse(bookValidator.IsFirstUpper(test));
        }

        [Test]
        public void ValidatorTest_IsFirstRussianUpper_True()
        {
            string test = "Первый символ прописной";

            Assert.IsTrue(bookValidator.IsFirstUpper(test));
        }

        [Test]
        public void ValidatorTest_IsFirstRussianUpper_False()
        {
            string test = "первый символ прописной";

            Assert.IsFalse(bookValidator.IsFirstUpper(test));
        }

        [Test]
        public void ValidatorTest_SpaceCheckerFirstLast_False()
        {
            string test = " space Checker ";

            Assert.IsFalse(bookValidator.SpaceChecker(test));
        }

        [Test]
        public void ValidatorTest_SpaceCheckerIsUpperAfter_False()
        {
            string test = "Space checker Space";

            Assert.IsFalse(bookValidator.SpaceChecker(test));
        }

        [Test]
        public void ValidatorTest_SpaceCheckerIsUpperAfter_True()
        {
            string test = "Space Checker";

            Assert.IsTrue(bookValidator.SpaceChecker(test));
        }

        [Test]
        public void ValidatorTest_IsHyphenOrApostropheFirstOrLast_False()
        {
            string test = "' test -";

            Assert.IsFalse(bookValidator.IsHyphenOrApostropheFirstOrLast(test));
        }

        [Test]
        public void ValidatorTest_IsHyphenOrApostropheFirstOrLast_True()
        {
            string test = "t-e's-t";

            Assert.IsTrue(bookValidator.IsHyphenOrApostropheFirstOrLast(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterSingleHyphen_False()
        {
            string test = "Гусь-хрустальный";

            Assert.IsFalse(bookValidator.IsUpperAfterSingleOrDoubleHyphen(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterSingleHyphen_True()
        {
            string test = "Гусь-Хрустальный";

            Assert.IsTrue(bookValidator.IsUpperAfterSingleOrDoubleHyphen(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterDoubleHyphen_False()
        {
            string test = "Ростов-на-дону";

            Assert.IsFalse(bookValidator.IsUpperAfterSingleOrDoubleHyphen(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterDoubleHyphen2_False()
        {
            string test = "Ростов-На-Дону";

            Assert.IsFalse(bookValidator.IsUpperAfterSingleOrDoubleHyphen(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterDoubleHyphen_True()
        {
            string test = "Ростов-на-Дону";

            Assert.IsTrue(bookValidator.IsUpperAfterSingleOrDoubleHyphen(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterApostrophe_False()
        {
            string test = "Д'артан'ьян";

            Assert.IsFalse(bookValidator.IsUpperAfterApostrophe(test));
        }

        [Test]
        public void ValidatorTest_IsUpperAfterApostrophe_True()
        {
            string test = "Д'Артаньян";

            Assert.IsTrue(bookValidator.IsUpperAfterApostrophe(test));
        }

        [Test]
        public void ValidatorTest_PublishingYearChecker_False()
        {
            Assert.IsFalse(bookValidator.PublishingYearChecker(3021));
        }

        [Test]
        public void ValidatorTest_PublishingYearChecker_True()
        {
            Assert.IsTrue(bookValidator.PublishingYearChecker(2021));
        }

        [Test]
        public void ValidatorTest_TwoLanguagesInString_False()
        {
            string test = "Русские symbols";

            Assert.IsFalse(bookValidator.IsOnlyOneLanguage(test));
        }

        [Test]
        public void ValidatorTest_LatinLanguageInString_True()
        {
            string test = "Latin symbols";

            Assert.IsTrue(bookValidator.IsOnlyOneLanguage(test));
        }

        [Test]
        public void ValidatorTest_RussianLanguageInString_True()
        {
            string test = "Русские символы";

            Assert.IsTrue(bookValidator.IsOnlyOneLanguage(test));
        }

        [Test]
        public void ValidatorTest_ISBNChecker_True()
        {
            string test = "ISBN 953-456-654-7";

            Assert.IsTrue(bookValidator.ISBNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISBNChecker2_True()
        {
            string test = "ISBN 2-1234567-6-X";

            Assert.IsTrue(bookValidator.ISBNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISBNChecker_False()
        {
            string test = "isbn 2-1234567-6-X";

            Assert.IsFalse(bookValidator.ISBNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISBNChecker2_False()
        {
            string test = "953-456-654-7";

            Assert.IsFalse(bookValidator.ISBNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISBNChecker3_False()
        {
            string test = "ISBN 99999-12345-678-9";

            Assert.IsFalse(bookValidator.ISBNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISSNChecker_False()
        {
            string test = "ISSN 1234-123A";

            Assert.IsFalse(newspaperValidator.ISSNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISSNChecker2_False()
        {
            string test = "ISSN 123A-1234";

            Assert.IsFalse(newspaperValidator.ISSNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISSNChecker3_False()
        {
            string test = "ISBN 1234-1234";

            Assert.IsFalse(newspaperValidator.ISSNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISSNChecker4_False()
        {
            string test = "ISSN 12341234";

            Assert.IsFalse(newspaperValidator.ISSNChecker(test));
        }

        [Test]
        public void ValidatorTest_ISSNChecker5_False()
        {
            string test = "1234-1234";

            Assert.IsFalse(newspaperValidator.ISSNChecker(test));
        }
    }
}