﻿using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;

namespace ROKO.Lib.BLL.Tests
{
    [TestFixture]
    public class UniqueCheckerTest
    {
        private ICatalogLogic _catalogLogic;
        private ICatalogUnitLogic<Book> _bookLogic;
        private ICatalogUnitLogic<Newspaper> _newspaperLogic;
        private ICatalogUnitLogic<Patent> _patentLogic;

        private IValidator<Book> bookValidator;
        private IUniqueChecker<Book> bookUniqueChecker;

        private IValidator<Newspaper> newspaperValidator;
        private IUniqueChecker<Newspaper> newspaperUniqueChecker;

        private IValidator<Patent> patentValidator;
        private IUniqueChecker<Patent> patentUniqueChecker;

        private static DateTime CorrectIssDate = new DateTime(2000, 1, 1);

        [SetUp]
        public void Startup()
        {
            var fakeLoggerCatalogDAO = new Mock<ILogger<CatalogDAO>>();
            var fakeLoggerBookDAO = new Mock<ILogger<BookDAO>>();
            var fakeLoggerNewspaperDAO = new Mock<ILogger<CatalogUnitDAO<Newspaper>>>();
            var fakeLoggerPatentDAO = new Mock<ILogger<CatalogUnitDAO<Patent>>>();

            var fakeLoggerBookValidator = new Mock<ILogger<BookValidator>>();
            var fakeLoggerNewspaperValidator = new Mock<ILogger<NewspaperValidator>>();
            var fakeLoggerPatentValidator = new Mock<ILogger<PatentValidator>>();

            var fakeLoggerCatalogLogic = new Mock<ILogger<CatalogLogic>>();
            var fakeLoggerBookLogic = new Mock<ILogger<BookLogic>>();
            var fakeLoggerCatalogUnitLogicNewspaper = new Mock<ILogger<CatalogUnitLogic<Newspaper>>>();
            var fakeLoggerCatalogUnitLogicPatent = new Mock<ILogger<CatalogUnitLogic<Patent>>>();
            var fakeLoggerBookUniqueChecker = new Mock<ILogger<BookUniqueChecker>>();
            var fakeLoggerNewspaperUniqueChecker = new Mock<ILogger<NewspaperUniqueChecker>>();
            var fakeLoggerPatentUniqueChecker = new Mock<ILogger<PatentUniqueChecker>>();

            var catalogDAO = new CatalogDAO(fakeLoggerCatalogDAO.Object);
            IBookDAO bookDAO = new BookDAO(fakeLoggerBookDAO.Object);
            ICatalogUnitDAO<Newspaper> newspaperDAO = new CatalogUnitDAO<Newspaper>(fakeLoggerNewspaperDAO.Object);
            ICatalogUnitDAO<Patent> patentDAO = new CatalogUnitDAO<Patent>(fakeLoggerPatentDAO.Object);

            bookValidator = new BookValidator(fakeLoggerBookValidator.Object);
            bookUniqueChecker = new BookUniqueChecker(bookDAO, fakeLoggerBookUniqueChecker.Object);

            newspaperValidator = new NewspaperValidator(fakeLoggerNewspaperValidator.Object);
            newspaperUniqueChecker = new NewspaperUniqueChecker(newspaperDAO, fakeLoggerNewspaperUniqueChecker.Object);

            patentValidator = new PatentValidator(fakeLoggerPatentValidator.Object);
            patentUniqueChecker = new PatentUniqueChecker(patentDAO, fakeLoggerPatentUniqueChecker.Object);

            _bookLogic = new BookLogic(bookDAO, bookValidator, bookUniqueChecker, fakeLoggerBookLogic.Object);
            _newspaperLogic = new CatalogUnitLogic<Newspaper>(newspaperDAO, newspaperValidator, newspaperUniqueChecker, fakeLoggerCatalogUnitLogicNewspaper.Object);
            _patentLogic = new CatalogUnitLogic<Patent>(patentDAO, patentValidator, patentUniqueChecker, fakeLoggerCatalogUnitLogicPatent.Object);
            _catalogLogic = new CatalogLogic(catalogDAO, fakeLoggerCatalogLogic.Object);
        }

        [Test]
        public void UniqueCheckerTest_Book_AddingISBN_True()
        {
            var CorrectBook = new Book("UniqueNameThree", "UniqueNameThree", "UniqueNameThree", 1994, 2, new List<Author> { new Author("Fname", "Sname") }, string.Empty, "ISBN 5-16-148410-0");

            var res = _bookLogic.Add(CorrectBook);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Book_AddingDifferentTitle_True()
        {
            var CorrectBook = new Book("UniqueNameThree", "UniqueNameThree", "UniqueNameThree", 1994, 2, new List<Author> { new Author("Fname", "Sname") });

            var res = _bookLogic.Add(CorrectBook);

            CorrectBook = new Book("UniqueNameTitle", "UniqueNameThree", "UniqueNameThree", 1994, 2, new List<Author> { new Author("Fname", "Sname") });

            res = _bookLogic.Add(CorrectBook);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Book_AddingDifferentDate_True()
        {
            var CorrectBook = new Book("UniqueNameThree", "UniqueNameThree", "UniqueNameThree", 1994, 2, new List<Author> { new Author("Fname", "Sname") });

            var res = _bookLogic.Add(CorrectBook);

            CorrectBook = new Book("UniqueNameThree", "UniqueNameThree", "UniqueNameThree", 2020, 2, new List<Author> { new Author("Fname", "Sname") });

            res = _bookLogic.Add(CorrectBook);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Newspaper_AddingISSN_True()
        {
            Newspaper CorrectNewspaper = new Newspaper("UniqueNameFive", "UniqueNameFive", "UniqueNameFive", 2000, 2, CorrectIssDate, string.Empty, -1, "ISSN 0021-0002");
            var res = _newspaperLogic.Add(CorrectNewspaper);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Newspaper_AddingDifferentTitle_True()
        {
            Newspaper CorrectNewspaper = new Newspaper("UniqueNameFive", "UniqueNameFive", "UniqueNameFive", 2000, 2, CorrectIssDate);
            var res = _newspaperLogic.Add(CorrectNewspaper);

            CorrectNewspaper = new Newspaper("UniqueNameTitle", "UniqueNameFive", "UniqueNameFive", 2000, 2, CorrectIssDate);
            res = _newspaperLogic.Add(CorrectNewspaper);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Newspaper_AddingDifferentPublishingHouse_True()
        {
            Newspaper CorrectNewspaper = new Newspaper("UniqueNameFive", "UniqueNameFive", "UniqueNameFive", 2000, 2, CorrectIssDate);
            var res = _newspaperLogic.Add(CorrectNewspaper);

            CorrectNewspaper = new Newspaper("UniqueNameFive", "UniqueNameFive", "UniqueNameHouse", 2000, 2, CorrectIssDate);
            res = _newspaperLogic.Add(CorrectNewspaper);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Newspaper_AddingDifferentDate_True()
        {
            Newspaper CorrectNewspaper = new Newspaper("UniqueNameFive", "UniqueNameFive", "UniqueNameFive", 2000, 2, CorrectIssDate);
            var res = _newspaperLogic.Add(CorrectNewspaper);

            CorrectNewspaper = new Newspaper("UniqueNameFive", "UniqueNameFive", "UniqueNameHouse", 1888, 2, new DateTime(1888, 1, 1));
            res = _newspaperLogic.Add(CorrectNewspaper);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Patent_AddingDifferentRegistrationNumber_True()
        {
            Patent CorrectPatent = new Patent("UniqueNameSix", "UniqueNameSix", 1, new DateTime(1995, 1, 1), 2);
            var res = _patentLogic.Add(CorrectPatent);

            CorrectPatent = new Patent("UniqueNameSix", "UniqueNameSix", 2, new DateTime(1995, 1, 1), 2);
            res = _patentLogic.Add(CorrectPatent);

            Assert.IsTrue(res > -1);
        }

        [Test]
        public void UniqueCheckerTest_Patent_AddingDifferentCountry_True()
        {
            Patent CorrectPatent = new Patent("UniqueNameSix", "UniqueNameSix", 1, new DateTime(1995, 1, 1), 2);
            var res = _patentLogic.Add(CorrectPatent);

            CorrectPatent = new Patent("UniqueNameSix", "UniqueNameSixNew", 1, new DateTime(1995, 1, 1), 2);
            res = _patentLogic.Add(CorrectPatent);

            Assert.IsTrue(res > -1);
        }
    }
}