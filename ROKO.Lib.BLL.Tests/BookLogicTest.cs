﻿using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROKO.Lib.BLL.Tests
{
    [TestFixture]
    public class BookLogicTest
    {
        private IBookLogic _bookLogic;
        private ICatalogLogic _catalogLogic;
        private IValidator<Book> bookValidator;
        private IUniqueChecker<Book> bookUniqueChecker;

        [SetUp]
        public void Startup()
        {
            var fakeLogger = new Mock<ILogger<BookDAO>>();
            var fakeLoggerBookValidator = new Mock<ILogger<BookValidator>>();
            var fakeLoggerBookUniqueChecker = new Mock<ILogger<BookUniqueChecker>>();
            var fakeLoggerBookLogic = new Mock<ILogger<BookLogic>>();
            var fakeLoggerCatalogLogic = new Mock<ILogger<CatalogLogic>>();

            IBookDAO bookDAO = new BookDAO(fakeLogger.Object);
            var catalogDAO = new CatalogDAO(fakeLogger.Object);

            bookValidator = new BookValidator(fakeLoggerBookValidator.Object);
            bookUniqueChecker = new BookUniqueChecker(bookDAO, fakeLoggerBookUniqueChecker.Object);

            _bookLogic = new BookLogic(bookDAO, bookValidator, bookUniqueChecker, fakeLoggerBookLogic.Object);
            _catalogLogic = new CatalogLogic(catalogDAO, fakeLoggerCatalogLogic.Object);
        }

        public readonly int IncorrectPublicationYear = 1,
                            IncorrectNumOfPages = -1,
                            CorrectPublicationYear = 1998,
                            CorrectNumOfPages = 1;

        public readonly string IncorrectName = "incorrect -458qqrname",
                               CorrectName = "Correctname";

        [Test]
        public void BookLogic_Add_WrongInputBook_False()
        {
            Book wrongBook = new Book(IncorrectName, IncorrectName, IncorrectName, IncorrectPublicationYear, IncorrectNumOfPages);

            Assert.AreEqual(_bookLogic.Add(wrongBook), -1);
        }

        [Test]
        public void BookLogic_Add_CorrectBook_True()
        {
            Book correctBook = new Book(CorrectName, CorrectName, CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            IEnumerable<Book> temp = _bookLogic.GetAllSpecified();

            Assert.AreEqual(_bookLogic.Add(correctBook), temp.Count());
        }

        [Test]
        public void BookLogic_Add_SameObject_False()
        {
            Book correctBook = new Book(CorrectName, CorrectName, CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            var res = _bookLogic.Add(correctBook);
            res = _bookLogic.Add(correctBook);

            Assert.AreEqual(res, -1);
        }

        [Test]
        public void BookLogic_Remove_True()
        {
            Book correctBook = new Book(CorrectName, CorrectName, CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            IEnumerable<Book> temp = _bookLogic.GetAllSpecified();

            _bookLogic.Add(correctBook);
            _catalogLogic.Remove(0);

            Assert.AreEqual(0, temp.Count());
        }

        [Test]
        public void BookLogic_Add_PubPlaceRussianSymbols_True()
        {
            Book correctBook = new Book("Title", "Москва", CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            Assert.IsTrue(_bookLogic.Add(correctBook) > -1);
        }

        [Test]
        public void BookLogic_Add_PubPlaceLatinSymbols_True()
        {
            Book correctBook = new Book("Title", "New York", CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            Assert.IsTrue(_bookLogic.Add(correctBook) > -1);
        }

        [Test]
        public void BookLogic_Add_DoubleHyphen_False()
        {
            Book correctBook = new Book("Title", "-New-York", CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            Assert.AreEqual(_bookLogic.Add(correctBook), -1);
        }

        [Test]
        public void BookLogic_Add_SingleHyphen_False()
        {
            Book correctBook = new Book("Title", "-New York", CorrectName, CorrectPublicationYear, CorrectNumOfPages);

            Assert.AreEqual(_bookLogic.Add(correctBook), -1);
        }

        [Test]
        public void BookLogic_Add_PublishingHouseTooLong_False()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 301; i++)
            {
                sb.Append('a');
            }

            var LongName = sb.ToString();

            Book incorrectBook = new Book(CorrectName, CorrectName, LongName, CorrectPublicationYear, CorrectNumOfPages);

            Assert.AreEqual(_bookLogic.Add(incorrectBook), -1);
        }

        [Test]
        public void BookLogic_Add_WrongNumberOfPages_False()
        {
            Book correctBook = new Book(CorrectName, CorrectName, CorrectName, CorrectPublicationYear, IncorrectNumOfPages);

            Assert.AreEqual(_bookLogic.Add(correctBook), -1);
        }

        [Test]
        public void BookLogic_Add_CorrectAuthors_True()
        {
            Book correctBook = new Book(CorrectName, CorrectName, CorrectName, CorrectPublicationYear, CorrectNumOfPages, new List<Author> { new Author("Fname", "Sname"), new Author("Erich", "Remark"), new Author("Lev", "Tolstoy") });

            Assert.IsTrue(_bookLogic.Add(correctBook) > -1);
        }

        [Test]
        public void BookLogic_Add_IncorrectAuthors_False()
        {
            Book incorrectBook = new Book(CorrectName, CorrectName, CorrectName, CorrectPublicationYear, CorrectNumOfPages, new List<Author> { new Author("fname", "sname") });

            Assert.AreEqual(_bookLogic.Add(incorrectBook), -1);
        }
    }
}