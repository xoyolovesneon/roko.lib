﻿using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROKO.Lib.BLL.Tests
{
    [TestFixture]
    public class PatentLogicTest
    {
        private ICatalogLogic _catalogLogic;
        private ICatalogUnitLogic<Patent> _patentLogic;

        private IValidator<Patent> patentValidator;
        private IUniqueChecker<Patent> patentUniqueChecker;

        [SetUp]
        public void Startup()
        {
            var fakeLoggerCatalogDAO = new Mock<ILogger<CatalogDAO>>();
            var fakeLoggerPatentDAO = new Mock<ILogger<CatalogUnitDAO<Patent>>>();

            var fakeLoggerPatentValidator = new Mock<ILogger<PatentValidator>>();
            var fakeLoggerPatentUniqueChecker = new Mock<ILogger<PatentUniqueChecker>>();
            var fakeLoggerCatalogUnitLogicPatent = new Mock<ILogger<CatalogUnitLogic<Patent>>>();
            var fakeLoggerCatalogLogic = new Mock<ILogger<CatalogLogic>>();

            var catalogDAO = new CatalogDAO(fakeLoggerCatalogDAO.Object);
            ICatalogUnitDAO<Patent> patentDAO = new CatalogUnitDAO<Patent>(fakeLoggerPatentDAO.Object);

            patentValidator = new PatentValidator(fakeLoggerPatentValidator.Object);
            patentUniqueChecker = new PatentUniqueChecker(patentDAO, fakeLoggerPatentUniqueChecker.Object);

            _patentLogic = new CatalogUnitLogic<Patent>(patentDAO, patentValidator, patentUniqueChecker, fakeLoggerCatalogUnitLogicPatent.Object);
            _catalogLogic = new CatalogLogic(catalogDAO, fakeLoggerCatalogLogic.Object);
        }

        private static int IncorrectYear = 1,
                            IncorrectNumOfPages = -1,
                            IncorrectRegNumber = -1,
                            CorrectYear = 1998,
                            CorrectNumOfPages = 1,
                            CorrectRegNumber = 1;

        private static string IncorrectName = "incorrect -458qqrname",
                              CorrectName = "Correctname";

        private static DateTime CorrectDate = new DateTime(CorrectYear, 1, 1),
                                IncorrectDate = new DateTime(IncorrectYear, 1, 1);

        [Test]
        public void PatentLogic_Add_WrongInputPatent_False()
        {
            Patent wrongPatent = new Patent(IncorrectName, IncorrectName, IncorrectRegNumber, IncorrectDate, IncorrectNumOfPages);

            Assert.AreEqual(_patentLogic.Add(wrongPatent), -1);
        }

        [Test]
        public void PatentLogic_Add_CorrectPatent_True()
        {
            Patent correctPatent = new Patent(CorrectName, CorrectName, CorrectRegNumber, CorrectDate, CorrectNumOfPages);

            IEnumerable<Patent> temp = _patentLogic.GetAllSpecified();

            Assert.AreEqual(_patentLogic.Add(correctPatent), temp.Count());
        }

        [Test]
        public void PatentLogic_Add_SameObject_False()
        {
            Patent correctPatent = new Patent(CorrectName, CorrectName, CorrectRegNumber, CorrectDate, CorrectNumOfPages);

            var res = _patentLogic.Add(correctPatent);
            res = _patentLogic.Add(correctPatent);

            Assert.AreEqual(res, -1);
        }

        [Test]
        public void PatentLogic_Add_PubPlaceRussianSymbols_True()
        {
            Patent correctPatent = new Patent("Title", "Москва", CorrectRegNumber, CorrectDate, CorrectNumOfPages);

            Assert.IsTrue(_patentLogic.Add(correctPatent) > -1);
        }

        [Test]
        public void PatentLogic_Add_PubPlaceLatinSymbols_True()
        {
            Patent correctPatent = new Patent("Title", "Los Angeles", CorrectRegNumber, CorrectDate, CorrectNumOfPages);

            Assert.IsTrue(_patentLogic.Add(correctPatent) > -1);
        }

        [Test]
        public void PatentLogic_PubPlaceAbbreviation_True()
        {
            Patent patent = new Patent(CorrectName, "USA", CorrectRegNumber, CorrectDate, CorrectNumOfPages);

            Assert.IsTrue(_patentLogic.Add(patent) > -1);
        }

        [Test]
        public void PatentLogic_Add_PlaceOfPublicationTooLong_False()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 201; i++)
            {
                sb.Append('a');
            }

            var LongName = sb.ToString();

            Patent incorrectPatent = new Patent(CorrectName, LongName, CorrectRegNumber, CorrectDate, CorrectNumOfPages);

            Assert.AreEqual(_patentLogic.Add(incorrectPatent), -1);
        }

        [Test]
        public void PatentLogic_WrongRegNumber_False()
        {
            Patent incorrectPatent = new Patent(CorrectName, CorrectName, 1234567890, CorrectDate, CorrectNumOfPages);

            Assert.AreEqual(_patentLogic.Add(incorrectPatent), -1);
        }

        [Test]
        public void PatentLogic_CorrectRegNumber_True()
        {
            Patent correctPatent = new Patent(CorrectName, CorrectName, 1234567, CorrectDate, CorrectNumOfPages);

            Assert.IsTrue(_patentLogic.Add(correctPatent) > -1);
        }

        [Test]
        public void PatentLogic_DatesConflict()
        {
            Patent incorrectPatent = new Patent(CorrectName, CorrectName, CorrectRegNumber, CorrectDate, CorrectNumOfPages, DateTime.Now);

            Assert.AreEqual(_patentLogic.Add(incorrectPatent), -1);
        }

        [Test]
        public void PatentLogic_DatesConflict2()
        {
            Patent incorrectPatent = new Patent(CorrectName, CorrectName, CorrectRegNumber, DateTime.Now, CorrectNumOfPages, new DateTime(1300, 1, 1));

            Assert.AreEqual(_patentLogic.Add(incorrectPatent), -1);
        }
    }
}