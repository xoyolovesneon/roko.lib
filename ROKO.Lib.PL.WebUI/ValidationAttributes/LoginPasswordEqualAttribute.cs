﻿using ROKO.Lib.PL.WebUI.Models;
using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.PL.WebUI.ValidationAttributes
{
    public class LoginPasswordEqualAttribute : ValidationAttribute
    {
        public LoginPasswordEqualAttribute()
        {
            ErrorMessage = "Логин не может быть частью пароля!";
        }

        public override bool IsValid(object value)
        {
            RegisterViewModel user = value as RegisterViewModel;

            if (user.Password.Contains(user.Login))
            {
                return false;
            }

            return true;
        }
    }
}