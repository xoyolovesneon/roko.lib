﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.PL.WebUI.Controllers
{
    public class PatentController : Controller
    {
        private ICatalogUnitLogic<Patent> _patentLogic;
        private IAuthorLogic _authorLogic;

        private ILogger<PatentController> _logger;

        public PatentController(ICatalogUnitLogic<Patent> patentLogic, IAuthorLogic authorLogic, ILogger<PatentController> logger)
        {
            _patentLogic = patentLogic;
            _authorLogic = authorLogic;

            _logger = logger;
        }

        // GET: PatentController
        public ActionResult Index()
        {
            IEnumerable<Patent> patents = _patentLogic.GetAllSpecified();
            return View(patents);
        }

        // GET: PatentController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PatentController/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            ViewBag.authors = _authorLogic.GetAuthors().Select(x => new SelectListItem { Value = x.AuthorID.ToString(), Text = x.FirstName + " " + x.LastName }).ToList();

            _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' opened Patent Creating view. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PatentController)} Method: {nameof(PatentController.Create)}");

            return View("CreatePatentWithAuthors");
        }

        // POST: PatentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Patent patent, IEnumerable<int> authors)
        {
            try
            {
                var patentId = _patentLogic.Add(patent);

                _authorLogic.AddAuthorsToLibraryObject(patentId, authors);

                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' added new patent with id = {patentId}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PatentController)} Method: {nameof(PatentController.Create)}");

                return RedirectToAction("Index", "Catalog");
            }
            catch(Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PatentController)} Method: {nameof(PatentController.Create)}");

                return View();
            }
        }

        // GET: PatentController/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PatentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Patent patent)
        {
            try
            {
                _patentLogic.Edit(patent);

                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' edited patent with id = {patent.Id}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PatentController)} Method: {nameof(PatentController.Edit)}");

                return RedirectToAction(nameof(Index), "Catalog");
            }
            catch(Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(PatentController)} Method: {nameof(PatentController.Edit)}");

                return View("EditErrorGet");
            }
        }

        // GET: PatentController/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PatentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}