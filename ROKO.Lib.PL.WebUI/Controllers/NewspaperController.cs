﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.Entities;
using ROKO.Lib.PL.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ROKO.Lib.PL.WebUI.Controllers
{
    public class NewspaperController : Controller
    {
        private ICatalogUnitLogic<Newspaper> _newspaperLogic;
        private ICatalogLogic _catalogLogic;

        private ILogger<NewspaperController> _logger;

        public NewspaperController(ICatalogUnitLogic<Newspaper> newspaperLogic, ICatalogLogic catalogLogic, ILogger<NewspaperController> logger)
        {
            _newspaperLogic = newspaperLogic;
            _catalogLogic = catalogLogic;

            _logger = logger;
        }

        // GET: NewspaperController
        public ActionResult Index()
        {
            IEnumerable<Newspaper> newspapers = _newspaperLogic.GetAllSpecified();
            return View(newspapers);
        }

        // GET: NewspaperController/Details/5
        public ActionResult Details(int id)
        {
            Newspaper newspaper = _newspaperLogic.GetAllSpecified().Where(n => n.Id == id).FirstOrDefault();

            NewspaperViewModel newspaperViewModel = new NewspaperViewModel();
            newspaperViewModel.CurrentNewspaper = newspaper;

            var allNewspapers = _newspaperLogic.GetAllSpecified();

            List<Newspaper> issueList = new List<Newspaper>();

            foreach (var newspaperToChoose in allNewspapers)
            {
                if (newspaper.ISSN != null && newspaper.ISSN == newspaperToChoose.ISSN)
                {
                    issueList.Add(newspaperToChoose);
                }

                if (newspaper.ISSN == null && newspaper.Title == newspaperToChoose.Title && newspaper.PublishingHouse == newspaperToChoose.PublishingHouse)
                {
                    issueList.Add(newspaperToChoose);
                }
            }

            newspaperViewModel.NewspaperIssues = issueList.OrderByDescending(n => n.IssueNumber);

            return View("NewspaperWithIssuesDetails", newspaperViewModel);
        }

        // GET: NewspaperController/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' opened Newspaper Creating view. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.Create)}");

            return View("CreateNewspaper");
        }

        // POST: NewspaperController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Newspaper newspaper)
        {
            try
            {
                var newspaperId = _newspaperLogic.Add(newspaper);
               
                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' added new newspaper with id = {newspaperId}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.Create)}");

                return RedirectToAction("Index", "Catalog");
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.Create)}");

                return View();
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult CreateIssue(int id)
        {
            var newspaper = _newspaperLogic.GetAllSpecified().Where(x => x.Id == id).FirstOrDefault();

            _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' opened CreateNewspaperIssue view with id = {id}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.CreateIssue)}");

            return View(newspaper);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateIssue(Newspaper inputNewspaperIssue)
        {
            try
            {
                Newspaper issueToDB = new Newspaper(
                    inputNewspaperIssue.Title,
                    inputNewspaperIssue.PlaceOfPublication,
                    inputNewspaperIssue.PublishingHouse,
                    (int)inputNewspaperIssue.PublicationOrApplicationYear,
                    inputNewspaperIssue.NumberOfPages,
                    inputNewspaperIssue.ReleaseDate,
                    inputNewspaperIssue.Note,
                    inputNewspaperIssue.IssueNumber,
                    inputNewspaperIssue.ISSN
                    );

                var issueId = _newspaperLogic.Add(issueToDB);

                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' added new newspaper issue with id = {issueId}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.CreateIssue)}");

                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.CreateIssue)}");

                return View();
            }
        }

        // GET: NewspaperController/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NewspaperController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Newspaper newspaper)
        {
            try
            {
                _newspaperLogic.Edit(newspaper);

                _logger.LogInformation($"Message: Admin with login '{User.Identity.Name}' edited newspaper with id = {newspaper.Id}. Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.Edit)}");

                return RedirectToAction(nameof(Index), "Catalog");
            }
            catch(Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} CurrentUser: {User.Identity.Name} Time: {DateTime.UtcNow} Layer: {nameof(PL)} Class: {nameof(NewspaperController)} Method: {nameof(NewspaperController.Edit)}");

                return View();
            }
        }

        // GET: NewspaperController/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: NewspaperController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}