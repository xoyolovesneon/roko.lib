﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.PL.WebUI.Models
{
    public class BookAddingWithAuthors
    {
        public Book Book { get; set; }

        public IEnumerable<Author> Authors { get; set; }
    }
}