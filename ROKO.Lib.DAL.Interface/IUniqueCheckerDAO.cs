﻿using ROKO.Lib.Entities;

namespace ROKO.Lib.DAL.Interface
{
    public interface IUniqueCheckerDAO<T> where T : LibraryObject
    {
        bool Check(T input);
    }
}