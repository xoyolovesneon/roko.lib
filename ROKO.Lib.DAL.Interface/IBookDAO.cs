﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.DAL.Interface
{
    public interface IBookDAO : ICatalogUnitDAO<Book>, IUniqueCheckerDAO<Book>
    {
        Dictionary<string, List<Book>> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate);
    }
}