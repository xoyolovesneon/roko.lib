﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.DAL.Interface
{
    public interface ICatalogUnitDAO<T> : IUniqueCheckerDAO<T> where T : LibraryObject
    {
        int Add(T unit);

        void Edit(T unit);

        IEnumerable<T> GetAllSpecified();
    }
}