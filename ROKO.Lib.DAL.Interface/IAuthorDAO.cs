﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.DAL.Interface
{
    public interface IAuthorDAO
    {
        IEnumerable<Author> GetAuthors();

        CatalogUnitWithAuthors AddAuthors(CatalogUnitWithAuthors unit);

        void AddAuthorsToLibraryObject(int libraryObjectId, IEnumerable<int> authorsIds);
    }
}