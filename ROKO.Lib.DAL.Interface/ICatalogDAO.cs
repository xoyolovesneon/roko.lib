﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.DAL.Interface
{
    public interface ICatalogDAO
    {
        bool Remove(int id);

        IEnumerable<LibraryObject> GetCatalog();

        IEnumerable<LibraryObject> GetRecordByTitle(string title);

        IEnumerable<LibraryObject> Sort(SortingCriterion criterion);

        LibraryObject FindCatalogElementById(int id);

        IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author);

        IEnumerable<LibraryObject> FindBookByAuthor(Author author);

        IEnumerable<LibraryObject> FindPatentByAuthor(Author author);

        Dictionary<int, List<LibraryObject>> GroupRecordsByYear();

        void SaveCatalogStorage();
    }
}