﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;

namespace ROKO.Lib.BLL
{
    public class UserLogic : IUserLogic
    {
        private IUserDAO _userDAO;

        private ILogger<UserLogic> _logger;

        public UserLogic(IUserDAO userDAO, ILogger<UserLogic> logger)
        {
            _userDAO = userDAO;
            _logger = logger;
        }

        public int AddUser(User user)
        {
            try
            {
                return _userDAO.AddUser(user);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.UserLogic)} Method: {nameof(BLL.UserLogic.AddUser)}");
                return -1;
            }
        }

        public void DeleteUser(int id)
        {
            try
            {
                _userDAO.DeleteUser(id);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.UserLogic)} Method: {nameof(BLL.UserLogic.DeleteUser)}");
            }
        }

        public IEnumerable<User> GetAll()
        {
            try
            {
                return _userDAO.GetAll();
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.UserLogic)} Method: {nameof(BLL.UserLogic.GetAll)}");
                return null;
            }
        }

        public User GetUserByLogin(string login)
        {
            try
            {
                return _userDAO.GetUserByLogin(login);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.UserLogic)} Method: {nameof(BLL.UserLogic.GetUserByLogin)}");
                return null;
            }
        }

        public void UpdateRole(User user)
        {
            try
            {
                _userDAO.UpdateRole(user);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.UserLogic)} Method: {nameof(BLL.UserLogic.UpdateRole)}");
            }
        }
    }
}