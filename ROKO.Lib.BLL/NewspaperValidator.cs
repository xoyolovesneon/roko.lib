﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.Entities;
using System;
using System.Text.RegularExpressions;

namespace ROKO.Lib.BLL
{
    public class NewspaperValidator : Validator<Newspaper>
    {
        private ILogger<Validator<Newspaper>> _logger;

        private int flag = 0;

        public NewspaperValidator(ILogger<Validator<Newspaper>> logger) : base(logger)
        {
            _logger = logger;
        }

        public override bool Check(Newspaper inp)
        {
            if (inp.Title.Length > TitleMaxLength)
            {
                _logger.LogError($"ErrorMessage: Title length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (!IsOnlyOneLanguage(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Publication place language error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (!IsFirstUpper(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Publication place starts with upper error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.PlaceOfPublication.Length > PlaceNameMaxLength)
            {
                _logger.LogError($"ErrorMessage: Publication place length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (!IsHyphenOrApostropheFirstOrLast(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Publication place first or last hyphen error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (!SpaceChecker(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Pubication place space error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.PublishingHouse.Length > TitleMaxLength)
            {
                _logger.LogError($"ErrorMessage: Publishing house length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.PublicationOrApplicationYear < MinPublicationYear)
            {
                _logger.LogError($"ErrorMessage: Publication Year < min publication year error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (PublishingYearChecker((int)inp.PublicationOrApplicationYear) == false)
            {
                _logger.LogError($"ErrorMessage: Publication Year > current year error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.ReleaseDate.Year != inp.PublicationOrApplicationYear)
            {
                _logger.LogError($"ErrorMessage: Issue Date do not matches with year of publication. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.NumberOfPages < 0)
            {
                _logger.LogError($"ErrorMessage: Number of pages error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.Note.Length > NoteMaxLength)
            {
                _logger.LogError($"ErrorMessage: Note length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            if (inp.ISSN != string.Empty && !ISSNChecker(inp.ISSN))
            {
                _logger.LogError($"ErrorMessage: ISSN error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.Check)}");
                flag++;
            }

            return flag == 0;
        }

        public bool ISSNChecker(string input)
        {
            try
            {
                Regex issn = new Regex(@"\b(ISSN)\b\s[0-9]{4}-[0-9]{4}");

                return (input.Length == 14 && issn.IsMatch(input));
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.NewspaperValidator)} Method: {nameof(BLL.NewspaperValidator.ISSNChecker)}");
                return false;
            }
        }
    }
}