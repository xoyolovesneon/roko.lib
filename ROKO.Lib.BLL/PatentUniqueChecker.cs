﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;

namespace ROKO.Lib.BLL
{
    public class PatentUniqueChecker : IUniqueChecker<Patent>
    {
        private readonly ICatalogUnitDAO<Patent> _patentSQLDao;

        private ILogger<PatentUniqueChecker> _logger;

        public PatentUniqueChecker(ICatalogUnitDAO<Patent> patentSQLDao, ILogger<PatentUniqueChecker> logger)
        {
            _patentSQLDao = patentSQLDao;
            _logger = logger;
        }

        public bool Check(Patent patent)
        {
            try
            {
                return _patentSQLDao.Check(patent);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.PatentUniqueChecker)} Method: {nameof(BLL.PatentUniqueChecker.Check)}");
                return false;
            }
        }
    }
}