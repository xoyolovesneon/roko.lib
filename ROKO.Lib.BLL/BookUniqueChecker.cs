﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;

namespace ROKO.Lib.BLL
{
    public class BookUniqueChecker : IUniqueChecker<Book>
    {
        private readonly IBookDAO _bookSQLDao;

        private ILogger<BookUniqueChecker> _logger;

        public BookUniqueChecker(IBookDAO bookSQLDao, ILogger<BookUniqueChecker> logger)
        {
            _bookSQLDao = bookSQLDao;
            _logger = logger;
        }

        public bool Check(Book book)
        {
            try
            {
                return _bookSQLDao.Check(book);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookUniqueChecker)} Method: {nameof(BLL.BookUniqueChecker.Check)}");
                return false;
            }
        }
    }
}