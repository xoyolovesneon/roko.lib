﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.Entities;
using System;
using System.Linq;

namespace ROKO.Lib.BLL
{
    public class AuthorValidator : Validator<Author>
    {
        private ILogger<Validator<Author>> _logger;

        public AuthorValidator()
        {
        }

        public AuthorValidator(ILogger<Validator<Author>> logger) : base(logger)
        {
            _logger = logger;
        }

        public override bool Check(Author inp)
        {
            try
            {
                if (inp.LastName == "")
                {
                    return IsOnlyOneLanguage(inp.FirstName)
                    && IsFirstUpper(inp.FirstName)
                    && inp.FirstName.Count() < FirstNameMaxLength
                    && IsHyphenOrApostropheFirstOrLast(inp.FirstName)
                    && IsUpperAfterSingleOrDoubleHyphen(inp.FirstName);
                }
                else
                {
                    return IsOnlyOneLanguage(inp.FirstName)
                      && IsFirstUpper(inp.FirstName)
                      && inp.FirstName.Count() < FirstNameMaxLength
                      && IsHyphenOrApostropheFirstOrLast(inp.FirstName)
                      && IsUpperAfterSingleOrDoubleHyphen(inp.FirstName)
                      && IsOnlyOneLanguage(inp.LastName)
                      && inp.LastName.Count() < LastNameMaxLength
                      && IsFirstUpper(inp.LastName)
                      && IsHyphenOrApostropheFirstOrLast(inp.LastName)
                      && IsUpperAfterSingleOrDoubleHyphen(inp.LastName)
                      && IsUpperAfterApostrophe(inp.LastName);
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.AuthorValidator)} Method: {nameof(BLL.AuthorValidator.Check)}");
                return false;
            }
        }
    }
}