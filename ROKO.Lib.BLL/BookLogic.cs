﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL.Interface;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;

namespace ROKO.Lib.BLL
{
    public class BookLogic : CatalogUnitLogic<Book>, IBookLogic
    {
        private IBookDAO _bookDAO;
        private IValidator<Book> _bookValidator;
        private IUniqueChecker<Book> _bookUniqueChecker;

        private ILogger<BookLogic> _logger;

        public BookLogic(IBookDAO bookDAO, IValidator<Book> bookValidator, IUniqueChecker<Book> bookUniqueChecker, ILogger<BookLogic> logger)
            : base(bookDAO, bookValidator, bookUniqueChecker, logger)
        {
            _bookDAO = bookDAO;
            _bookValidator = bookValidator;
            _bookUniqueChecker = bookUniqueChecker;

            _logger = logger;
        }

        public Dictionary<string, List<Book>> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate)
        {
            try
            {
                return _bookDAO.FindAllByPublisherNameTemplateAndGroupByPublisher(publisherNameTemplate);
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookLogic)} Method: {nameof(BLL.BookLogic.FindAllByPublisherNameTemplateAndGroupByPublisher)}");
                return null;
            }
        }
    }
}