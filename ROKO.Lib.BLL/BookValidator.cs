﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.Entities;
using System;
using System.Text.RegularExpressions;

namespace ROKO.Lib.BLL
{
    public class BookValidator : Validator<Book>
    {
        private AuthorValidator _authorValidator;

        private ILogger<Validator<Book>> _logger;

        public BookValidator(ILogger<Validator<Book>> logger) : base(logger)
        {
            _authorValidator = new AuthorValidator();
            _logger = logger;
        }

        public override bool Check(Book inp)
        {
            int flag = 0;

            if (inp.Title.Length > TitleMaxLength)
            {
                _logger.LogError($"ErrorMessage: Title length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (!IsOnlyOneLanguage(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Place of publication language error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (!IsFirstUpper(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Place of publication first upper letter error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (inp.PlaceOfPublication.Length > PlaceNameMaxLength)
            {
                _logger.LogError($"ErrorMessage: Place of publication length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (!IsHyphenOrApostropheFirstOrLast(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Place of publication first or last hyphen error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (!SpaceChecker(inp.PlaceOfPublication))
            {
                _logger.LogError($"ErrorMessage: Place of publication spaces error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (IsUpperAfterSingleOrDoubleHyphen(inp.PlaceOfPublication) == false)
            {
                _logger.LogError($"ErrorMessage: Place of publication double hyphen error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (inp.PublishingHouse.Length > TitleMaxLength)
            {
                _logger.LogError($"ErrorMessage: Publishing house length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (inp.PublicationOrApplicationYear < MinPublicationYear)
            {
                _logger.LogError($"ErrorMessage: Publication year < min publication year error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (PublishingYearChecker((int)inp.PublicationOrApplicationYear) == false)
            {
                _logger.LogError($"ErrorMessage: Publication Year > current year error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (inp.NumberOfPages < 0)
            {
                _logger.LogError($"ErrorMessage: Number of pages error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (inp.Note.Length > NoteMaxLength)
            {
                _logger.LogError($"ErrorMessage: Note length error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            if (!ISBNChecker(inp.ISBN))
            {
                _logger.LogError($"ErrorMessage: ISBN error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                flag++;
            }

            var authors = inp.Authors;

            if (authors != null)
            {
                for (int i = 0; i < authors.Count; i++)
                {
                    if (!_authorValidator.Check(authors[i]))
                    {
                        _logger.LogError($"ErrorMessage: Authors error. Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.Check)}");
                        flag++;
                    }
                }
            }

            return flag == 0;
        }

        public bool ISBNChecker(string input)
        {
            try
            {
                if (input == "")
                {
                    return true;
                }

                Regex isbn = new Regex(@"\b(ISBN)\b\s([0-7]|8[0-9]|9[0-4]|9[5-8][0-9]|99[0-3]|99[4-8][0-9]|999[0-9][0-9])-\d{1,7}-\d{1,7}-[0-9|X]");

                return (input.Length == 18 && isbn.IsMatch(input));
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(BLL)} Class: {nameof(BLL.BookValidator)} Method: {nameof(BLL.BookValidator.ISBNChecker)}");
                return false;
            }
        }
    }
}