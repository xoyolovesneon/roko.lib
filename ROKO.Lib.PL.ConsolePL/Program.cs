﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ROKO.Lib.BLL;
using ROKO.Lib.DAL;
using Serilog;

namespace ROKO.Lib.PL.ConsolePL
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args);

            host.ConfigureServices((services) =>
            {
                services
                        .AddSQLDao()
                        .AddLogic();
                services.AddHostedService<Worker>();
            });

            host.ConfigureLogging((hostingContext, loggingBuilder) =>
            {
                loggingBuilder.ClearProviders();

                var logger = new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .WriteTo.File("data.log")
                        //.WriteTo.Console()
                        .CreateLogger();

                loggingBuilder.AddSerilog(logger, dispose: true);
            });

            return host;
        }
    }
}