﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.Entities
{
    public class CatalogUnitWithAuthors : LibraryObject
    {
        [Required]
        public List<Author> Authors { get; set; }

        private List<Author> authors = new List<Author>();

        public CatalogUnitWithAuthors() : base()
        {
        }

        public CatalogUnitWithAuthors(string inpTitle, List<Author> author, int inpPubYear, int inpNumOfPages, string inpNote)
            : base(inpTitle, inpPubYear, inpNumOfPages, inpNote)
        {
            if (author != null)
            {
                foreach (var item in author)
                {
                    AddAuthor(item);
                }
            }
        }

        protected void AddAuthor(Author author)
        {
            if (!authors.Contains(author))
            {
                authors.Add(author);
            }
        }
    }
}