﻿using System.Collections.Generic;

namespace ROKO.Lib.Entities
{
    internal class AuthorComparator : IComparer<Author>
    {
        public int Compare(Author x, Author y)
        {
            if (x.AuthorID == y.AuthorID)
            {
                return 0;
            }

            if (x.FirstName.CompareTo(y.FirstName) != 0)
            {
                return x.FirstName.CompareTo(y.FirstName);
            }
            else if (x.LastName.CompareTo(y.LastName) != 0)
            {
                return x.LastName.CompareTo(y.LastName);
            }

            return 0;
        }
    }
}