﻿using System.ComponentModel.DataAnnotations;

namespace ROKO.Lib.Entities
{
    public class LibraryObject
    {
        public LibraryObject()
        {
        }

        public LibraryObject(string inpTitle, int inpPubYear, int inpNumOfPages, string inpNote, int Id = 0, string Type = "")
        {
            Title = inpTitle;
            PublicationOrApplicationYear = inpPubYear;
            NumberOfPages = inpNumOfPages;
            Note = inpNote;
            this.Id = Id;
            this.Type = Type;
        }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        public string Type { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "Длина поля не должна превышать 300 символов!")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Publication Year")]
        //[Range(4, 4, ErrorMessage = "Год состоит из 4 цифр в формате: уууу!")]
        public int? PublicationOrApplicationYear { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Количество страниц не может быть меньше 0!")]
        public int NumberOfPages { get; set; }

        [StringLength(2000, ErrorMessage = "Длина поля не должна превышать 2000 символов!")]
        public string Note { get; set; }

        public override string ToString()
        {
            return $"Id: {Id} | Type: {Type} | Title: {Title} | PublicationOrApplicationYear: {PublicationOrApplicationYear} | NumberOfPages: {NumberOfPages} | Note: {Note}";
        }
    }
}