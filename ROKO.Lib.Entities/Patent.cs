﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ROKO.Lib.Entities
{
    public class Patent : CatalogUnitWithAuthors
    {
        private DateTime? _applicationDate;

        [Required]
        [MaxLength(200, ErrorMessage = "Длина поля не должна превышать 200 символов!")]
        public string Country { get; set; }

        [Required]
        [Range(0, 999999999, ErrorMessage = "Номер не может состоять из более чем 9 цифр!")]
        public int RegistrationNumber { get; set; }

        //[Range(typeof(DateTime), "01.01.1474", "01.01.3000", ErrorMessage = "Год не может быть меньше, чем 1474!")]
        public DateTime? ApplicationDate
        {
            get => _applicationDate;
            set
            {
                if (value == null)
                {
                    PublicationOrApplicationYear = null;
                    _applicationDate = value;
                }
                else
                {
                    PublicationOrApplicationYear = value.Value.Year;
                    _applicationDate = value.Value;
                }
            }
        }

        [Required]
        //[Range(typeof(DateTime), "01.01.1474", "01.01.3000", ErrorMessage = "Год не может быть меньше, чем 1474!")]
        public DateTime PublicationDate { get; set; }

        public Patent()
        {
        }

        public Patent(string inpTitle, string country, int regNumber, DateTime pubDate, int inpNumOfPages,
            DateTime? appDate = null, List<Author> author = null, string inpNote = "", int inpPubYear = 0)
            : base(inpTitle, author, inpPubYear, inpNumOfPages, inpNote)
        {
            Title = inpTitle;
            Authors = author;
            Country = country;
            RegistrationNumber = regNumber;
            ApplicationDate = appDate;
            PublicationDate = pubDate;
            NumberOfPages = inpNumOfPages;
            Note = inpNote;
        }

        private StringBuilder InventorList()
        {
            if (Authors.Count == 0)
            {
                return new StringBuilder("There is no author!");
            }
            else
            {
                StringBuilder result = new StringBuilder();

                foreach (var item in Authors)
                {
                    result.Append($"{item}, ");
                }
                result.Remove(result.Length - 2, 2);
                return result;
            }
        }

        public override string ToString()
        {
            return $"Id: {Id} | Type: {Type} | Title: {Title} | Inventors: {InventorList()} | Country: {Country} | RegistrationNumber: {RegistrationNumber} | ApplicationDate: {ApplicationDate.Value.ToShortDateString()} | PublicationDate: {PublicationDate.ToShortDateString()} | NumberOfPages: {NumberOfPages} | Note: {Note}";
        }
    }
}