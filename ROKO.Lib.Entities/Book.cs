﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ROKO.Lib.Entities
{
    public class Book : CatalogUnitWithAuthors
    {
        [Required]
        [MaxLength(200, ErrorMessage = "Длина поля не должна превышать 200 символов!")]
        public string PlaceOfPublication { get; set; }

        [Required]
        [MaxLength(300, ErrorMessage = "Длина поля не должна превышать 300 символов!")]
        public string PublishingHouse { get; set; }

        [RegularExpression(@"\b(ISBN)\b\s([0-7]|8[0-9]|9[0-4]|9[5-8][0-9]|99[0-3]|99[4-8][0-9]|999[0-9][0-9])-\d{1,7}-\d{1,7}-[0-9|X]", ErrorMessage = "ISBN должен удовлетворять формату записи!")]
        public string ISBN { get; set; }

        public Book(string inpTitle, string inpPlaceOfPub, string inpPubHouse,
            int inpPubYear, int inpNumOfPages, List<Author> author = null, string inpNote = "", string isbn = "")
            : base(inpTitle, author, inpPubYear, inpNumOfPages, inpNote)
        {
            Title = inpTitle;
            Authors = author;
            PlaceOfPublication = inpPlaceOfPub;
            PublishingHouse = inpPubHouse;
            PublicationOrApplicationYear = inpPubYear;
            NumberOfPages = inpNumOfPages;
            Note = inpNote;
            ISBN = isbn;
        }

        public Book() : base()
        {
        }

        public StringBuilder AuthorList()
        {
            if (Authors.Count == 0)
            {
                return new StringBuilder("No Name");
            }
            else
            {
                StringBuilder result = new StringBuilder();

                foreach (var item in Authors)
                {
                    result.Append($"{item.FirstName[0]}. {item.LastName}, ");
                }
                result.Remove(result.Length - 2, 2);
                return result;
            }
        }

        public override string ToString()
        {
            return $"Id: {Id} | Type: {Type} | Title: {Title} | Authors: {AuthorList()} | PlaceOfPublication: {PlaceOfPublication} | PublishingHouse: {PublishingHouse} | PublicationOrApplicationYear: {PublicationOrApplicationYear} | NumberOfPages: {NumberOfPages} | Note: {Note} | ISBN: {ISBN}";
        }
    }
}