﻿namespace ROKO.Lib.BLL.Interface
{
    public interface IValidator<T>
    {
        bool Check(T input);
    }
}