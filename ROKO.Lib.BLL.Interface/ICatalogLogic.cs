﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.BLL.Interface
{
    public interface ICatalogLogic
    {
        bool Remove(int id);

        IEnumerable<LibraryObject> GetCatalog();

        IEnumerable<LibraryObject> GetRecordByTitle(string title);

        IEnumerable<LibraryObject> Sort(SortingCriterion criterion);

        LibraryObject FindCatalogElementById(int id);

        IEnumerable<LibraryObject> FindBookByAuthor(Author author);

        IEnumerable<LibraryObject> FindPatentByAuthor(Author author);

        IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author);

        Dictionary<int, List<LibraryObject>> GroupRecordsByYear();
    }
}