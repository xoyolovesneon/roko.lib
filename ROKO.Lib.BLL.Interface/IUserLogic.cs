﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.BLL.Interface
{
    public interface IUserLogic
    {
        IEnumerable<User> GetAll();

        User GetUserByLogin(string login);

        int AddUser(User user);

        void DeleteUser(int id);

        void UpdateRole(User user);
    }
}