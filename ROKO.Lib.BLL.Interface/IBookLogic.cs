﻿using ROKO.Lib.Entities;
using System.Collections.Generic;

namespace ROKO.Lib.BLL.Interface
{
    public interface IBookLogic : ICatalogUnitLogic<Book>
    {
        Dictionary<string, List<Book>> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate);
    }
}