﻿using Microsoft.Extensions.DependencyInjection;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;

namespace ROKO.Lib.DAL
{
    public static class FakeDaoConfigurator
    {
        public static IServiceCollection AddFakeDAO(this IServiceCollection services)
        {
            return services
                           .AddTransient<ICatalogDAO, CatalogDAO>()
                           .AddTransient<IBookDAO, BookDAO>()
                           .AddTransient<ICatalogUnitDAO<Newspaper>, CatalogUnitDAO<Newspaper>>()
                           .AddTransient<ICatalogUnitDAO<Patent>, CatalogUnitDAO<Patent>>();
        }
    }
}