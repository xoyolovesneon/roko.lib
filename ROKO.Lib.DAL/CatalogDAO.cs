﻿using Microsoft.Extensions.Logging;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ROKO.Lib.DAL
{
    public class CatalogDAO : ICatalogDAO
    {
        private static List<LibraryObject> _catalog = new List<LibraryObject>();

        private static List<LibraryObject> libraryObjectsRepository;//ReadCatalogStorage();

        private static int _catalogId = 0;

        private ILogger<CatalogDAO> _logger;

        public CatalogDAO(ILogger<CatalogDAO> logger)
        {
            _logger = logger;
        }

        protected int Add(LibraryObject libraryObject)
        {
            libraryObject.Id = _catalogId;
            _catalogId++;
            _catalog.Add(libraryObject);

            //libraryObjectsRepository.Add(libraryObject);
            //SaveCatalogStorage();

            _logger.LogInformation($"Library object {libraryObject.GetType().Name} is successfully added.");
            return _catalogId;
        }

        public bool Remove(int id)
        {
            var item = _catalog.FirstOrDefault(p => p.Id == id);
            if (item != null)
            {
                _catalog.Remove(item);
                _logger.LogInformation($"Library object with ID={id} is successfully removed.");
                return true;
            }
            _logger.LogInformation("Library object did not removed.");
            return false;
        }

        public IEnumerable<LibraryObject> Sort(SortingCriterion criterion)
        {
            return criterion.Equals(SortingCriterion.ASCPUBLICATIONYEAR) ? AscSort() : DescSort();
        }

        public IEnumerable<LibraryObject> AscSort()
        {
            _logger.LogInformation("Catalog sorted in ascending order.");
            return _catalog.OrderBy(n => n.PublicationOrApplicationYear);
        }

        public IEnumerable<LibraryObject> DescSort()
        {
            _logger.LogInformation("Catalog sorted in descending order.");
            return _catalog.OrderByDescending(n => n.PublicationOrApplicationYear);
        }

        public IEnumerable<LibraryObject> GetCatalog()
        {
            return _catalog;
        }

        public IEnumerable<LibraryObject> GetRecordByTitle(string inputTitle)
        {
            _logger.LogInformation($"Record(s) with Title={inputTitle} printed.");
            return _catalog.Where(p => p.Title.Contains(inputTitle));
        }

        public Dictionary<int, List<LibraryObject>> GroupRecordsByYear()
        {
            var temp = GetCatalog().GroupBy(x => x.PublicationOrApplicationYear);

            Dictionary<int, List<LibraryObject>> res = new Dictionary<int, List<LibraryObject>>();

            foreach (var item in temp)
            {
                foreach (var item2 in item)
                {
                    if (!res.ContainsKey((int)item.Key))
                    {
                        res.Add((int)item.Key, new List<LibraryObject>());
                    }

                    res[(int)item.Key].Add(item2);
                }
            }

            _logger.LogInformation("Records grouped by year.");
            return res;
        }

        public IEnumerable<LibraryObject> FindBookByAuthor(Author author)
        {
            var tempLib = GetCatalog().Where(x => x is Book);

            List<Book> result = new List<Book>();

            foreach (var item in tempLib)
            {
                var tempLib2 = (item as Book).Authors;
                foreach (var jtem in tempLib2)
                {
                    if (jtem.FirstName == author.FirstName && jtem.LastName == author.LastName)
                    {
                        result.Add(item as Book);
                    }
                }
            }

            _logger.LogInformation($"Records with Authors={author} printed.");
            return result;
        }

        public IEnumerable<LibraryObject> FindPatentByAuthor(Author author)
        {
            var tempLib = GetCatalog().Where(x => x is Patent);
            List<Patent> result = new List<Patent>();
            foreach (var item in tempLib)
            {
                var tempLib2 = (item as Patent).Authors;
                foreach (var jtem in tempLib2)
                {
                    if (jtem.FirstName == author.FirstName && jtem.LastName == author.LastName)
                    {
                        result.Add(item as Patent);
                    }
                }
            }

            _logger.LogInformation($"Records with Authors={author} printed.");
            return result;
        }

        public IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author)
        {
            var tempLib = GetCatalog().Where(x => x is CatalogUnitWithAuthors);
            List<CatalogUnitWithAuthors> result = new List<CatalogUnitWithAuthors>(0);
            foreach (var item in tempLib)
            {
                var tempLib2 = (item as CatalogUnitWithAuthors).Authors;
                foreach (var jtem in tempLib2)
                {
                    if (jtem.FirstName == author.FirstName && jtem.LastName == author.LastName)
                    {
                        result.Add(item as CatalogUnitWithAuthors);
                    }
                }
            }

            _logger.LogInformation($"Books and Patents with Authors={author} printed.");
            return result;
        }

        private static List<LibraryObject> ReadCatalogStorage()
        {
            List<LibraryObject> tempRepo = new List<LibraryObject>();

            if (!File.Exists(@"C:\Users\ROKO000000160\Source\Repos\roko_lib\CatalogRepository.txt"))
            {
                return tempRepo;
            }

            using (StreamReader sr = new StreamReader(@"C:\Users\ROKO000000160\Source\Repos\roko_lib\CatalogRepository.txt"))
            {
                while (!sr.EndOfStream)
                {
                    string className = sr.ReadLine();

                    if (className == "Book")
                    {
                        Type bookType = typeof(Book);

                        var book = new Book();

                        List<Author> authorList = new List<Author>();

                        Dictionary<string, string> parsedPropertyAndValueByColon = new Dictionary<string, string>();

                        var arrayOfBookProperties = book.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                        string stringPropertyColonValue = sr.ReadLine();

                        while (stringPropertyColonValue.IndexOf(':') != -1)
                        {
                            if (stringPropertyColonValue.StartsWith("Authors:"))
                            {
                                authorList = AuthorParser(sr);
                            }
                            else
                            {
                                int colonPos = stringPropertyColonValue.IndexOf(':');

                                string property = stringPropertyColonValue.Substring(0, colonPos);

                                string value = stringPropertyColonValue.Substring(colonPos + 1);

                                parsedPropertyAndValueByColon.Add(property, value);

                                stringPropertyColonValue = sr.ReadLine();
                            }
                        }

                        foreach (var bookProperty in arrayOfBookProperties)
                        {
                            foreach (var parsedProperty in parsedPropertyAndValueByColon)
                            {
                                if (bookProperty.GetType().Name == parsedProperty.Key)
                                {
                                    PropertyInfo propertyInfo = bookType.GetProperty(bookProperty.Name);
                                    propertyInfo.SetValue(book, parsedProperty.Value);
                                }

                                if (bookProperty.GetValue(book) is List<Author>)
                                {
                                    PropertyInfo propertyInfo = bookType.GetProperty(bookProperty.Name);
                                    propertyInfo.SetValue(book, authorList);
                                }
                            }
                        }
                    }
                }
            }

            return tempRepo;
        }

        public static List<Author> AuthorParser(StreamReader sr)
        {
            List<Author> authorList = new List<Author>();

            string temp = sr.ReadLine();

            while (!temp.StartsWith("Id:"))
            {
                int authorID = int.Parse(temp);
                temp = sr.ReadLine();

                string authorFirstName = temp;
                temp = sr.ReadLine();

                string authorLastName = temp;

                authorList.Add(new Author(authorFirstName, authorLastName));

                temp = sr.ReadLine();
            }

            return authorList;
        }

        public void SaveCatalogStorage()
        {
            using (StreamWriter streamWriter = new StreamWriter(@"C:\Users\ROKO000000160\Source\Repos\roko_lib\CatalogRepository.txt"))
            {
                StringBuilder output = new StringBuilder();

                foreach (var libraryObject in libraryObjectsRepository)
                {
                    if (libraryObject is Book book)
                    {
                        var arrayOfProperties = book.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                        output.Append($"{book.GetType().Name}{Environment.NewLine}");

                        foreach (var property in arrayOfProperties)
                        {
                            var a = property.GetValue(book);
                            output.Append($"{property.Name}:");
                            if (a is List<Author> authorList && authorList is not null && authorList.Any())
                            {
                                output.Append(Environment.NewLine);
                                var authorProperties = typeof(Author).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                foreach (var author in authorList)
                                {
                                    foreach (var authorProperty in authorProperties)
                                    {
                                        output.Append($"{authorProperty.Name}:{authorProperty.GetValue(author)}{Environment.NewLine}");
                                    }
                                }
                            }
                            else
                            {
                                output.Append($"{property.GetValue(book)}{Environment.NewLine}");
                            }
                        }
                    }

                    if (libraryObject is Newspaper newspaper)
                    {
                        var arrayOfProperties = newspaper.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                        output.Append($"{newspaper.GetType().Name}{Environment.NewLine}");

                        foreach (var property in arrayOfProperties)
                        {
                            output.Append($"{property.Name}:{property.GetValue(newspaper)}{Environment.NewLine}");
                        }
                    }

                    if (libraryObject is Patent patent)
                    {
                        var arrayOfProperties = patent.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

                        streamWriter.WriteLine($"{patent.GetType()}");

                        foreach (var property in arrayOfProperties)
                        {
                            streamWriter.WriteLine($"{property.Name}:{property.GetValue(patent)}");
                        }
                    }
                }

                streamWriter.WriteLine(output);
            }
        }

        public LibraryObject FindCatalogElementById(int id)
        {
            throw new NotImplementedException();
        }
    }
}