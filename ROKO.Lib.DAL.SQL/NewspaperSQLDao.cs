﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ROKO.Lib.DAL.SQL
{
    public class NewspaperSQLDao : ICatalogUnitDAO<Newspaper>, IUniqueCheckerDAO<Newspaper>
    {
        private IConfiguration _configuration;

        private ILogger<NewspaperSQLDao> _logger;

        public NewspaperSQLDao(IConfiguration configuration, ILogger<NewspaperSQLDao> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public int Add(Newspaper unit)
        {
            var unitType = typeof(Newspaper).Name;
            int unitId = -1;

            var result = new List<LibraryObject>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("AddNewspaper", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", unit.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterLibObjType = new SqlParameter("@LibraryObjectType", unitType);
                    command.Parameters.Add(parameterLibObjType);

                    SqlParameter parameterTitle = new SqlParameter("@Title", unit.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterPlaceOfPublication = new SqlParameter("@PlaceOfPublication", unit.PlaceOfPublication);
                    command.Parameters.Add(parameterPlaceOfPublication);

                    SqlParameter parameterPublishingHouse = new SqlParameter("@PublishingHouse", unit.PublishingHouse);
                    command.Parameters.Add(parameterPublishingHouse);

                    SqlParameter parameterPubOrAppYear = new SqlParameter("@PublicationOrApplicationYear", unit.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPubOrAppYear);

                    SqlParameter parameterNumberOfPages = new SqlParameter("@NumberOfPages", unit.NumberOfPages);
                    command.Parameters.Add(parameterNumberOfPages);

                    SqlParameter parameterReleaseDate = new SqlParameter("@ReleaseDate", unit.ReleaseDate);
                    command.Parameters.Add(parameterReleaseDate);

                    SqlParameter parameterNote = new SqlParameter("@Note", unit.Note);
                    command.Parameters.Add(parameterNote);

                    SqlParameter parameterIssueNumber = new SqlParameter("@IssueNumber", unit.IssueNumber);
                    command.Parameters.Add(parameterIssueNumber);

                    SqlParameter parameterISSN = new SqlParameter("@ISSN", unit.ISSN);
                    command.Parameters.Add(parameterISSN);

                    connection.Open();

                    command.Parameters["@Id"].Direction = System.Data.ParameterDirection.Output;

                    var reader = command.ExecuteNonQuery();

                    unitId = (int)command.Parameters["@Id"].Value;

                    unit.Id = unitId;

                    command.Dispose();
                }

                return unitId;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.NewspaperSQLDao)} Method: {nameof(DAL.SQL.NewspaperSQLDao.Add)}");
                return -1;
            }
        }

        public bool Check(Newspaper newspaper)
        {
            try
            {
                bool returnValue = false;

                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("NewspaperUniqueChecker", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterTitle = new SqlParameter("@Title", newspaper.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterPublishingHouse = new SqlParameter("@PublishingHouse", newspaper.PublishingHouse);
                    command.Parameters.Add(parameterPublishingHouse);

                    SqlParameter parameterReleaseDate = new SqlParameter("@ReleaseDate", newspaper.ReleaseDate);
                    command.Parameters.Add(parameterReleaseDate);

                    SqlParameter parameterISSN = new SqlParameter("@ISSN", newspaper.ISSN);
                    command.Parameters.Add(parameterISSN);

                    SqlParameter parameterReturnValue = new SqlParameter("@return_value", System.Data.SqlDbType.Bit);
                    command.Parameters.Add(parameterReturnValue);

                    connection.Open();

                    command.Parameters["@return_value"].Direction = System.Data.ParameterDirection.ReturnValue;

                    var reader = command.ExecuteNonQuery();

                    returnValue = Convert.ToBoolean(command.Parameters["@return_value"].Value);
                }

                if (returnValue == true)
                {
                    Console.WriteLine("There are no matches. Thats all good, thoe!");
                    return true;
                }
                else
                {
                    Console.WriteLine("There is a collision, element with this creds is already exists!");
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.NewspaperSQLDao)} Method: {nameof(DAL.SQL.NewspaperSQLDao.Check)}");
                return false;
            }
        }

        public void Edit(Newspaper newspaper)
        {
            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("EditNewspaper", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", newspaper.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterTitle = new SqlParameter("@Title", newspaper.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterPlaceOfPublication = new SqlParameter("@PlaceOfPublication", newspaper.PlaceOfPublication);
                    command.Parameters.Add(parameterPlaceOfPublication);

                    SqlParameter parameterPublishingHouse = new SqlParameter("@PublishingHouse", newspaper.PublishingHouse);
                    command.Parameters.Add(parameterPublishingHouse);

                    SqlParameter parameterPubOrAppYear = new SqlParameter("@PublicationOrApplicationYear", newspaper.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPubOrAppYear);

                    SqlParameter parameterNumberOfPages = new SqlParameter("@NumberOfPages", newspaper.NumberOfPages);
                    command.Parameters.Add(parameterNumberOfPages);

                    SqlParameter parameterReleaseDate = new SqlParameter("@ReleaseDate", newspaper.ReleaseDate);
                    command.Parameters.Add(parameterReleaseDate);

                    SqlParameter parameterNote = new SqlParameter("@Note", newspaper.Note);
                    command.Parameters.Add(parameterNote);

                    SqlParameter parameterIssueNumber = new SqlParameter("@IssueNumber", newspaper.IssueNumber);
                    command.Parameters.Add(parameterIssueNumber);

                    SqlParameter parameterISSN = new SqlParameter("@ISSN", newspaper.ISSN);
                    command.Parameters.Add(parameterISSN);

                    connection.Open();

                    var reader = command.ExecuteNonQuery();

                    command.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.NewspaperSQLDao)} Method: {nameof(DAL.SQL.NewspaperSQLDao.Edit)}");
            }
        }

        public IEnumerable<Newspaper> GetAllSpecified()
        {
            var result = new List<Newspaper>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("GetNewspapers", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Newspaper newspaper = new Newspaper(
                            (string)reader["Title"],
                            (string)reader["PlaceOfPublication"],
                            (string)reader["PublishingHouse"],
                            (int)reader["PublicationOrApplicationYear"],
                            (int)reader["NumberOfPages"],
                            (DateTime)reader["ReleaseDate"],
                            (string)reader["Note"],
                            (int)reader["IssueNumber"],
                            (string)reader["ISSN"]
                            );
                            newspaper.Id = (int)reader["LibraryObjectId"];
                            newspaper.Type = (string)reader["LibraryObjectType"];

                            result.Add(newspaper);
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.NewspaperSQLDao)} Method: {nameof(DAL.SQL.NewspaperSQLDao.GetAllSpecified)}");
                return null;
            }
        }
    }
}