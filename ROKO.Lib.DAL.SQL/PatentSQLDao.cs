﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ROKO.Lib.DAL.Interface;
using ROKO.Lib.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ROKO.Lib.DAL.SQL
{
    public class PatentSQLDao : ICatalogUnitDAO<Patent>, IUniqueCheckerDAO<Patent>
    {
        private IConfiguration _configuration;

        private ILogger<PatentSQLDao> _logger;

        public PatentSQLDao(IConfiguration configuration, ILogger<PatentSQLDao> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public int Add(Patent unit)
        {
            var unitType = typeof(Patent).Name;
            int unitId = -1;

            var result = new List<LibraryObject>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("AddPatent", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", unit.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterLibObjType = new SqlParameter("@LibraryObjectType", unitType);
                    command.Parameters.Add(parameterLibObjType);

                    SqlParameter parameterTitle = new SqlParameter("@Title", unit.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterCountry = new SqlParameter("@Country", unit.Country);
                    command.Parameters.Add(parameterCountry);

                    SqlParameter parameterRegNumber = new SqlParameter("@RegistrationNumber", unit.RegistrationNumber);
                    command.Parameters.Add(parameterRegNumber);

                    SqlParameter parameterPubOrAppYear = new SqlParameter("@PublicationOrApplicationYear", unit.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPubOrAppYear);

                    SqlParameter parameterNumberOfPages = new SqlParameter("@NumberOfPages", unit.NumberOfPages);
                    command.Parameters.Add(parameterNumberOfPages);

                    SqlParameter parameterApplicationDate = new SqlParameter("@ApplicationDate", unit.ApplicationDate);
                    command.Parameters.Add(parameterApplicationDate);

                    SqlParameter parameterPublicationDate = new SqlParameter("@PublicationDate", unit.PublicationDate);
                    command.Parameters.Add(parameterPublicationDate);

                    SqlParameter parameterNote = new SqlParameter("@Note", unit.Note);
                    command.Parameters.Add(parameterNote);

                    connection.Open();

                    command.Parameters["@Id"].Direction = System.Data.ParameterDirection.Output;

                    var reader = command.ExecuteNonQuery();

                    unitId = (int)command.Parameters["@Id"].Value;

                    unit.Id = unitId;

                    command.Dispose();
                }

                return unitId;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.PatentSQLDao)} Method: {nameof(DAL.SQL.PatentSQLDao.Add)}");
                return -1;
            }
        }

        public bool Check(Patent patent)
        {
            try
            {
                bool returnValue = false;

                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("PatentUniqueChecker", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterCountry = new SqlParameter("@Country", patent.Country);
                    command.Parameters.Add(parameterCountry);

                    SqlParameter parameterRegNumber = new SqlParameter("@RegistrationNumber", patent.RegistrationNumber);
                    command.Parameters.Add(parameterRegNumber);

                    SqlParameter parameterReturnValue = new SqlParameter("@return_value", System.Data.SqlDbType.Bit);
                    command.Parameters.Add(parameterReturnValue);

                    connection.Open();

                    command.Parameters["@return_value"].Direction = System.Data.ParameterDirection.ReturnValue;

                    var reader = command.ExecuteNonQuery();

                    returnValue = Convert.ToBoolean(command.Parameters["@return_value"].Value);
                }

                if (returnValue == true)
                {
                    Console.WriteLine("There are no matches. Thats all good, thoe!");
                    return true;
                }
                else
                {
                    Console.WriteLine("There is a collision, element with this creds is already exists!");
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.PatentSQLDao)} Method: {nameof(DAL.SQL.PatentSQLDao.Check)}");
                return false;
            }
        }

        public void Edit(Patent patent)
        {
            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("EditPatent", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameterId = new SqlParameter("@Id", patent.Id);
                    command.Parameters.Add(parameterId);

                    SqlParameter parameterTitle = new SqlParameter("@Title", patent.Title);
                    command.Parameters.Add(parameterTitle);

                    SqlParameter parameterCountry = new SqlParameter("@Country", patent.Country);
                    command.Parameters.Add(parameterCountry);

                    SqlParameter parameterRegNumber = new SqlParameter("@RegistrationNumber", patent.RegistrationNumber);
                    command.Parameters.Add(parameterRegNumber);

                    SqlParameter parameterPubOrAppYear = new SqlParameter("@PublicationOrApplicationYear", patent.PublicationOrApplicationYear);
                    command.Parameters.Add(parameterPubOrAppYear);

                    SqlParameter parameterNumberOfPages = new SqlParameter("@NumberOfPages", patent.NumberOfPages);
                    command.Parameters.Add(parameterNumberOfPages);

                    SqlParameter parameterApplicationDate = new SqlParameter("@ApplicationDate", patent.ApplicationDate);
                    command.Parameters.Add(parameterApplicationDate);

                    SqlParameter parameterPublicationDate = new SqlParameter("@PublicationDate", patent.PublicationDate);
                    command.Parameters.Add(parameterPublicationDate);

                    SqlParameter parameterNote = new SqlParameter("@Note", patent.Note);
                    command.Parameters.Add(parameterNote);

                    connection.Open();

                    var reader = command.ExecuteNonQuery();

                    command.Dispose();
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.PatentSQLDao)} Method: {nameof(DAL.SQL.PatentSQLDao.Edit)}");
            }
        }

        public IEnumerable<Patent> GetAllSpecified()
        {
            var result = new List<Patent>();

            try
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = new SqlCommand("GetPatents", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        var currentId = -1;
                        var indexOfLast = -1;
                        while (reader.Read())
                        {
                            if (currentId == (int)reader["LibraryObjectId"])
                            {
                                var authorFirstName = (string)reader["FirstName"];
                                var authorLastName = (string)reader["LastName"];

                                if (authorFirstName != null && authorLastName != null)
                                {
                                    Author author = new Author(authorFirstName, authorLastName);
                                    author.AuthorID = (int)reader["AuthorId"];
                                    result[indexOfLast].Authors.Add(author);
                                }
                            }
                            else
                            {
                                Patent patent = new Patent(
                                (string)reader["Title"],
                                (string)reader["Country"],
                                (int)reader["RegistrationNumber"],
                                (DateTime)reader["PublicationDate"],
                                (int)reader["NumberOfPages"],
                                (DateTime?)reader["ApplicationDate"],
                                new List<Author>(),
                                (string)reader["Note"],
                                (int)reader["PublicationOrApplicationYear"]
                                );
                                patent.Id = (int)reader["LibraryObjectId"];
                                patent.Type = (string)reader["LibraryObjectType"];

                                var authorFirstName = Convert.IsDBNull(reader["FirstName"]) ? null : (string)reader["FirstName"];
                                var authorLastName = Convert.IsDBNull(reader["LastName"]) ? null : (string)reader["LastName"];

                                if (authorFirstName != null && authorLastName != null)
                                {
                                    Author author = new Author(authorFirstName, authorLastName);
                                    author.AuthorID = (int)reader["AuthorId"];
                                    patent.Authors.Add(author);
                                }

                                currentId = patent.Id;

                                result.Add(patent);

                                indexOfLast = result.IndexOf(patent);
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"ErrorMessage: {e.Message} Time: {DateTime.UtcNow} Layer: {nameof(DAL)} Class: {nameof(DAL.SQL.PatentSQLDao)} Method: {nameof(DAL.SQL.PatentSQLDao.GetAllSpecified)}");
                return null;
            }
        }
    }
}